describe "POST /equipos/{equipo_id}/bookings" do
  before (:all) do
    payload = { email: "penelope@gmail.com", password: "qaninja" }
    result = Sessions.new.login(payload)
    @penelope_id = result.parsed_response["_id"]
  end

  context "solicitar locacao" do
    before (:all) do

      #dado que Isaque tenha uma Fender para Locação
      result = Sessions.new.login(email: "isaque@gmail.com", password: "qaninja")
      isaque_id = result.parsed_response["_id"]

      fender = {
        thumbnail: Helpers::get_thumb("fender-sb.jpg"),
        name: "Fender",
        category: "Cordas",
        price: 100,
      }
      MongoDB.new.remove_equipo(fender[:name], isaque_id)

      #e eu tenho o id do equipamento
      equipo = Equipos.new.create(fender, isaque_id)
      fender_id = equipo.parsed_response["_id"]

      #quando solicito a locação do equipamento

      @result = Equipos.new.booking(fender_id, @penelope_id)
    end
    it "deve retornar 200" do
      expect(@result.code).to eql 200
    end

    it "booking deve ter tamanho 24" do
      expect(@result.parsed_response["booking_id"].length).to eql 24
    end
  end
end
