#encoding: utf-8

describe "delete/equipos/{equipo_id}" do
  before (:all) do
    @payload = { email: "isaque@gmail.com", password: "qaninja" }

    result = Sessions.new.login(@payload)
    @user_id = result.parsed_response["_id"]
  end

  context "deletar equipo" do
    before (:all) do
      #dado qu eu tenho um novo equipamento
      @payload = {
        thumbnail: Helpers::get_thumb("pedais.jpg"),
        name: "Pedais",
        category: "Áudio e Tecnologia".force_encoding("ASCII-8BIT"),
        price: 120,
      }

      MongoDB.new.remove_equipo(@payload[:name], @user_id)

      #e eu tenho o id do equipamento
      equipo = Equipos.new.create(@payload, @user_id)
      @equipo_id = equipo.parsed_response["_id"]

      #quando faço uma requisição delete por id
      @result = Equipos.new.delete_by_id(@equipo_id, @user_id)
    end

    it "deve retornar 204" do
      expect(@result.code).to eql 204
    end
  end

  context "equipo nao existe" do
    before (:all) do
      @result = Equipos.new.delete_by_id(MongoDB.new.get_mongo_id, @user_id)
    end

    it "deve retornar 204" do
      expect(@result.code).to eql 204
    end
  end
end
