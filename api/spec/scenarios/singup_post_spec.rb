describe "POST /signup" do
  context "novo usuario" do
    before (:all) do
      payload = { name: "Isaque", email: "isaque@gmail.com", password: "qaninja" }

      MongoDB.new.remove_user(payload[:email])

      @result = Singup.new.create(payload)
    end

    it "valida status code" do
      expect(@result.code).to eql 200
    end

    it "valida id do usuario" do
      expect(@result.parsed_response["_id"].length).to eql 24
    end
  end

  context "usuario ja existente" do
    before (:all) do
      payload = { name: "Isaque Silverio", email: "isaque@hotmail.com", password: "qaninja" }
      MongoDB.new.remove_user(payload[:email])
      Singup.new.create(payload)
      @result = Singup.new.create(payload)
    end

    it "valida status code" do
      expect(@result.code).to eql 409
    end

    it "valida mensagem de erro" do
      expect(@result.parsed_response["error"]).to eql "Email already exists :("
    end

    it "valida codigo do erro" do
      expect(@result.parsed_response["code"]).to eql 1001
    end
  end

  examples = [
    {
      title: "campo nome obrigatorio",
      payload: { email: "isaques@gmail.com", password: "isaque" },
      code: 412,
      error: "required name",
    },
    {
      title: "campo email obrigatorio",
      payload: { name: "Isaque", email: "", password: "qaninja" },
      code: 412,
      error: "required email",
    },
    {
      title: "campo senha obrigatorio",
      payload: { name: "Isaque", email: "isaque@hotmail.com", password: "" },
      code: 412,
      error: "required password",
    },
  ]

  examples.each do |e|
    context "#{e[:title]}" do
      before (:all) do
        Singup.new.create(e[:payload])
        MongoDB.new.remove_user(e[:payload])
        @result = Singup.new.create(e[:payload])
      end
      it "valida status code #{e[:code]}" do
        expect(@result.code).to eql e[:code]
      end

      it "valida id do erro" do
        expect(@result.parsed_response["error"]).to eql e[:error]
      end
    end
  end
end
