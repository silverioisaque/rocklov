describe "POST /sessions" do
  context "login com sucesso" do
    before (:all) do
      payload = { email: "betao@hotmail.com", password: "qaninja" }
      @result = Sessions.new.login(payload)
    end

    it "valida status code" do
      expect(@result.code).to eql 200
    end

    it "valida id do usuario" do
      expect(@result.parsed_response["_id"].length).to eql 24
    end
  end

  examples = Helpers::get_fixture("login")

  # examples = [
  #   {
  #     title: "Email não existente",
  #     payload: { email: "teste@gmail.com", password: "12345678" },
  #     code: 401,
  #     error: "Unauthorized",
  #   },
  #   {
  #     title: "Senha em branco",
  #     payload: { email: "isaque@gmail.com", password: "" },
  #     code: 412,
  #     error: "required password",
  #   },
  #   {
  #     title: "Sem o campo senha",
  #     payload: { email: "isaque@gmail.com" },
  #     code: 412,
  #     error: "required password",
  #   },
  #   {
  #     title: "Senha errada",
  #     payload: { email: "isaque@gmail.com", password: "1234556" },
  #     code: 401,
  #     error: "Unauthorized",
  #   },
  #   {
  #     title: "Email em branco",
  #     payload: { email: "", password: "1234556" },
  #     code: 412,
  #     error: "required email",
  #   },
  #   {
  #     title: "Sem email",
  #     payload: { password: "1234556" },
  #     code: 412,
  #     error: "required email",
  #   },
  # ]

  examples.each do |e|
    context "#{e[:title]}" do
      before (:all) do
        @result = Sessions.new.login(e[:payload])
      end
      it "valida status code #{e[:code]}" do
        expect(@result.code).to eql e[:code]
      end

      it "valida id do erro" do
        expect(@result.parsed_response["error"]).to eql e[:error]
      end
    end
  end
end
