describe "get/equipos/{equipo_id}" do
  before (:all) do
    @payload = { email: "isaque@gmail.com", password: "qaninja" }
    result = Sessions.new.login(@payload)
    @user_id = result.parsed_response["_id"]
  end

  context "obter unico equipo" do
    before (:all) do
      #dado qu eu tenho um novo equipamento
      @payload = {
        thumbnail: Helpers::get_thumb("sanfona.jpg"),
        name: "Sanfona",
        category: "Outros",
        price: 499,
      }

      MongoDB.new.remove_equipo(@payload[:name], @user_id)

      #e eu tenho o id do equipamento
      equipo = Equipos.new.create(@payload, @user_id)
      @equipo_id = equipo.parsed_response["_id"]

      #quando faço uma requisição get_id
      @result = Equipos.new.find_by_id(@equipo_id, @user_id)
    end

    it "deve retornar 200" do
      expect(@result.code).to eql 200
    end

    it "deve retornar o nome" do
      expect(@result.parsed_response).to include("name" => @payload[:name])
    end
  end
end

context "equipo nao existe" do
  before (:all) do
    @result = Equipos.new.find_by_id(MongoDB.new.get_mongo_id, @user_id)
  end

  it "deve retornar 401" do
    expect(@result.code).to eql 401
  end
end

describe "get/equipos/" do
  before (:all) do
    @payloads = { email: "penelope@gmail.com", password: "qaninja" }
    results = Sessions.new.login(@payloads)
    @user = results.parsed_response["_id"]
  end

  context "obter uma lista" do
    before(:all) do
      #dado que eu tenho uma lista de equipamentos
      payloads = [
        {
          thumbnail: Helpers::get_thumb("violino.jpg"),
          name: "Violino",
          category: "Outros",
          price: 145,
        },
        {
          thumbnail: Helpers::get_thumb("mic.jpg"),
          name: "Microfone",
          category: "Outros",
          price: 300,
        },
      ]

      payloads.each do |payload|
        MongoDB.new.remove_equipo(payload[:name], @user)
        Equipos.new.create(payload, @user)
      end

      #quando faço uma requisição get para /equipos
      @results = Equipos.new.list(@user)
    end

    it "deve retornar 200" do
      expect(@results.code).to eql 200
    end

    it "deve retornar uma lista de equipos" do
      expect(@results.parsed_response).not_to be_empty
    end
  end
end
