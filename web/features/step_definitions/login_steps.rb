Dado('que acesso a página principal') do
    @login_page = LoginPage.new
    @login_page.open
end
  
Quando('submeto minhas credenciais com {string} e {string}') do |email, password|
    login_page = LoginPage.new

    login_page.open
    login_page.with(email, password)
end

Quando('submeto minhas credenciais:') do |table|

     user = table.hashes.first

    find("#user_email").set user[:email]
    find("#user_password").set user[:senha]

    click_button "Entrar"
end


