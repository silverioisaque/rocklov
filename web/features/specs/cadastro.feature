#language: pt

Funcionalidade: Cadastro
    Sendo um músico que possui equipamentos musicais
    Quero fazer o meu cadastro no RockLov
    Para que eu possa disponibilizá-los para locação

    @cadastro
    Cenario: Fazer cadastro
        Dado que acesso a página de cadastro
        Quando submeto o seguinte formulário de cadastro:
            | nome   | email                     | senha   |
            | Isaque | isaque.silverio@gmail.com | qaninja |
        Então sou redirecionado ao Dashboard

    Esquema do Cenario: Tentativa do Cadastro

        Dado que acesso a página de cadastro
        Quando submeto o seguinte formulário de cadastro:
            | nome         | email         | senha         |
            | <nome_input> | <email_input> | <senha_input> |
        Então vejo a mensagem de alerta: "<mensagem_output>"

        Exemplos:
            | nome_input | email_input               | senha_input | mensagem_output                  |
            |            | isaque.silverio@gmail.com | qaninja     | Oops. Informe seu nome completo! |
            | Isaque     |                           | qaninja     | Oops. Informe um email válido!   |
            | Isaque     | isaque.silverio@gmail.com |             | Oops. Informe sua senha secreta! |
            | Isaque     | isaque.silverio&gmail.com | qaninja     | Oops. Informe um email válido!   |
