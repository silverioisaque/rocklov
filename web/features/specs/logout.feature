#language: pt

Funcionalidade: Logout
    Sendo um usuário cadastrado
    Quero acessar o sistema da RockLov
    Após utilizar o sistema, quero deslogar

    
    Cenario: Logout do usuário

        Dado que acesso a página principal
        Quando submeto minhas credenciais com "isaque@gmail.com" e "qaninja"
        Então sou redirecionado ao Dashboard

    @temp
    Esquema do Cenario: Tentativa de Login

        Dado que acesso a página principal
        Quando submeto minhas credenciais:
            | email         | senha         |
            | <email_input> | <senha_input> |
        Então vejo a mensagem de alerta: "<mensagem_output>"

        Exemplos:
            | email_input         | senha_input | mensagem_output                  |
            | isaque@gmail.com    | teste       | Usuário e/ou senha inválidos.    |
            | isaque@yahoo.com.br | qaninja     | Usuário e/ou senha inválidos.    |
            | isaque*gmail.com    |             | Oops. Informe um email válido!   |
            |                     | qaninja     | Oops. Informe um email válido!   |
            | isaque@gmail.com    |             | Oops. Informe sua senha secreta! |