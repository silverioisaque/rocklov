#language: pt

Funcionalidade: Remover anuncios
    Sendo um anunciante que possui um equipamento indesejado
    Quero poder remover esse anuncio
    Para que eu possa manter meu Dashboard atualizado

    Contexto: Login
        * Login com "spider@gmail.com" e "qaninja"
    
    Cenario: Remover um anuncio

        Dado que eu tenho um anúncio indesejado
            | thumb     | telecaster.jpg |
            | nome      | Telecaster     |
            | categoria | Cordas         |
            | preco     | 200            |
        Quando eu solicito a exclusão desse item
            E confirmo a exclusão
        Então não devo ver esse item no meu Dashboard
    @teste
    Cenario: Desistir da exclusão

        Dado que eu tenho um anúncio indesejado
            | thumb     | conga.jpg |
            | nome      | Conga     |
            | categoria | Outros    |
            | preco     | 100       |
        Quando eu solicito a exclusão desse item
            Mas não confirmo a solicitação
        Então esse item dever permancer no meu Dashboard