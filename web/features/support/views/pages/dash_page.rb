class DashPage
  include Capybara::DSL

  def on_dash?
    page.has_css?(".dashboard")
  end

  def goto_eqipo_form
    click_button "Criar anúncio"
  end

  def equipo_list
    return anuncios = find(".equipo-list")
  end

  def solicito_exclusao(name)
    equipo = find(".equipo-list li", text: name)
    return equipo.find(".delete-icon").click
  end

  def confirm_remove
    return click_on "Sim"
  end

  def has_no_equipo?(name)
    return page.has_no_css?(".equipo-list li", text: name)
  end

  def not_confirm_remove
    return click_on "Não"
  end

  def order
    return find(".notifications p")
  end

  def order_actions(name)
    return page.has_css?(".notifications button", text: name)
  end
end
